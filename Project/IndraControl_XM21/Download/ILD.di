﻿<?xml version="1.0" encoding="UTF-8"?>
<DiagnosisCompileData ChangeId="25" Guid="abcbcd36-a145-4416-a516-795da42188a0" DataFormat="2.5.0">
  <DeletedIds>
    <Change ID="0" />
    <Change ID="1" />
    <Change ID="2" />
    <Change ID="3" />
    <Change ID="4" />
    <Change ID="5" />
    <Change ID="6" />
    <Change ID="7" />
    <Change ID="8" />
    <Change ID="9" />
    <Change ID="10" />
    <Change ID="11" />
    <Change ID="12" />
    <Change ID="13" />
    <Change ID="14" />
    <Change ID="15" />
    <Change ID="16" />
    <Change ID="17" />
    <Change ID="18" />
    <Change ID="19" />
    <Change ID="20" />
    <Change ID="21" />
    <Change ID="22" />
    <Change ID="23" />
    <Change ID="24" />
    <Change ID="25" />
  </DeletedIds>
  <Poes>
    <Poe Name="AS_HANDGRIFF" Type="1" Language="4" NameOrg="AS_Handgriff" Namespace="Application" Checksum="699732608" DataFormat="1.2.0">
      <ID />
      <Instances>
        <Instance Name="AS_HANDGRIFF" NameOrg="AS_Handgriff" Namespace="Application">
          <ID>0</ID>
        </Instance>
      </Instances>
      <ProviMessages />
      <Declaration DataFormat="1.0.0">
        <UsedVariable DataFormat="2.4.0">
          <Variable ID="5" Name="#poe_instance#.ba_auto" Type="72" Comment="" Address="" UserType="False" DeclArea="VAR" Len="0" NameOrg="#poe_instance_org#.BA_Auto" Namespace="Application" />
          <Variable ID="9" Name="#poe_instance#.b_zylendl_gs" Type="72" Comment="Rückmeldung Vakuum aufgebaut RM_Vak_an: BOOL;  Zylinder in Grundstellung" Address="" UserType="False" DeclArea="VAR" Len="0" NameOrg="#poe_instance_org#.b_ZylEndl_GS" Namespace="Application" />
          <Variable ID="10" Name="#poe_instance#.b_vak_an" Type="72" Comment=" Vakuum einschalten" Address="" UserType="False" DeclArea="VAR" Len="0" NameOrg="#poe_instance_org#.b_Vak_an" Namespace="Application" />
          <Variable ID="11" Name="#poe_instance#.b_ba_auto_akt" Type="72" Comment="Variablen  Betriebsart Automatik aktiv" Address="" UserType="False" DeclArea="VAR" Len="0" NameOrg="#poe_instance_org#.b_BA_Auto_akt" Namespace="Application" />
          <Variable ID="12" Name="#poe_instance#.rm_vak_an" Type="72" Comment="" Address="" UserType="False" DeclArea="VAR" Len="0" NameOrg="#poe_instance_org#.RM_Vak_an" Namespace="Application" />
          <Variable ID="13" Name="#poe_instance#.isfc.enable" Type="72" Comment="SFC is only actively processed, if Enable is pending. If Enable is missing, the SFC is stopped in the state." Address="" UserType="False" DeclArea="VAR_INPUT" Len="0" NameOrg="#poe_instance_org#.isfc.Enable" Namespace="Application" />
          <Variable ID="14" Name="#poe_instance#.isfc.start" Type="72" Comment="SFC rising edge allowed executing" Address="" UserType="False" DeclArea="VAR_INPUT" Len="0" NameOrg="#poe_instance_org#.isfc.Start" Namespace="Application" />
        </UsedVariable>
      </Declaration>
      <Detail>
        <Implementation>
          <Code />
        </Implementation>
        <Actions>
          <Name Name="aUserIn" Checksum="1218637006">
            <Code />
          </Name>
          <Name Name="Aufb_Vak" Checksum="2674115817">
            <Code />
          </Name>
          <Name Name="permActiv" Checksum="2269167644">
            <Code />
          </Name>
        </Actions>
      </Detail>
      <Sfc Module="1" DiagType="Nothing" Typ="ILDSfc02" StepMaxTime="" DataFormat="1.0.0" ID="0">
        <Steps>
          <Element Name="Init" ID="0">
            <FreezeVarTimeOut>
              <Variable ID="1" />
              <Variable ID="2" />
              <Variable ID="3" />
            </FreezeVarTimeOut>
            <Actions>
              <association actionname="aUserIn" qualifier="N" />
            </Actions>
            <Transitions>
              <Element Name="TRUE" ID="4">
                <EST_Code>TRUE</EST_Code>
              </Element>
            </Transitions>
          </Element>
          <Element Name="Pruefung_BA" ID="1" Comment="Überprüfung Betriebsart">
            <FreezeVarTimeOut>
              <Variable ID="1" />
              <Variable ID="2" />
              <Variable ID="3" />
              <Variable ID="5" />
              <Variable ID="11" />
            </FreezeVarTimeOut>
            <Actions />
            <Transitions>
              <Element Name="BA_Auto" ID="7">
                <EST_Code>b_BA_Auto_akt;</EST_Code>
              </Element>
            </Transitions>
          </Element>
          <Element Name="Aufbau_Vak" ID="2" Comment="Aufbau von Vakuum">
            <FreezeVarTimeOut>
              <Variable ID="1" />
              <Variable ID="2" />
              <Variable ID="3" />
              <Variable ID="10" />
              <Variable ID="12" />
            </FreezeVarTimeOut>
            <Actions>
              <association actionname="Aufb_Vak" qualifier="N" />
            </Actions>
            <Transitions>
              <Element Name="RM_Vak_an" ID="6">
                <EST_Code>b_Vak_an;</EST_Code>
              </Element>
            </Transitions>
          </Element>
        </Steps>
        <Transitions>
          <Element Name="_Init_to_Pruefung_BA" ID="2" />
          <Element Name="Init__to__Pruefung_BA" ID="4" />
          <Element Name="_Aufbau_Vak_to_Init" ID="0" />
          <Element Name="_Pruefung_BA_to_Aufbau_Vak" ID="1" />
          <Element Name="Aufbau_Vak__to__Init" ID="6" />
          <Element Name="Pruefung_BA__to__Aufbau_Vak" ID="7" />
        </Transitions>
        <MonErrors />
        <MachineStateSFC>
          <Home />
          <Start />
        </MachineStateSFC>
        <Instances>
          <Instance ID="0" Module="1" />
        </Instances>
        <ESTCodeSfc>
          <Action Name="AUSERIN">
            <flags>
            </flags>
            <MessagesSfc>
              <EST_Code>b_ZylEndl_GS := TRUE;</EST_Code>
            </MessagesSfc>
            <Bwa>
            </Bwa>
          </Action>
          <Action Name="AUFB_VAK">
            <flags>
            </flags>
            <MessagesSfc>
              <EST_Code>b_Vak_an := TRUE;</EST_Code>
            </MessagesSfc>
            <Bwa>
            </Bwa>
          </Action>
          <Action Name="PERMACTIV">
            <flags>
            </flags>
            <MessagesSfc>
              <EST_Code>isfc.Enable := TRUE;isfc.Start := TRUE;</EST_Code>
            </MessagesSfc>
            <Bwa>
            </Bwa>
          </Action>
        </ESTCodeSfc>
      </Sfc>
    </Poe>
    <Poe Name="PLCPROG" Type="1" Language="0" NameOrg="PlcProg" Namespace="Application" Checksum="533065090" DataFormat="1.2.0">
      <ID />
      <Instances>
        <Instance Name="PLCPROG" NameOrg="PlcProg" Namespace="Application">
          <ID>0</ID>
        </Instance>
      </Instances>
      <ProviMessages />
      <Declaration DataFormat="1.0.0">
        <UsedVariable DataFormat="2.4.0" />
      </Declaration>
      <Detail>
        <Implementation>
          <Code />
        </Implementation>
        <Actions />
      </Detail>
    </Poe>
    <Poe Name="HANDGRIFF" Type="1" Language="0" NameOrg="Handgriff" Namespace="Application" Checksum="1723298272" DataFormat="1.2.0">
      <ID />
      <Instances>
        <Instance Name="HANDGRIFF" NameOrg="Handgriff" Namespace="Application">
          <ID>0</ID>
        </Instance>
      </Instances>
      <ProviMessages />
      <Declaration DataFormat="1.0.0">
        <UsedVariable DataFormat="2.4.0" />
      </Declaration>
      <Detail>
        <Implementation>
          <Code />
        </Implementation>
        <Actions />
      </Detail>
    </Poe>
  </Poes>
  <GlobalMachineState />
  <GlobalVariables DataFormat="1.0.0">
    <UsedVariable DataFormat="2.4.0">
      <Variable ID="1" Name="sfcstatemanual" Type="72" Comment="" Address="" UserType="False" DeclArea="VAR_GLOBAL" Len="0" NameOrg="SFCStateManual" Namespace="Application.GVL_ILD2G" />
      <Variable ID="2" Name="sfcstateauto" Type="72" Comment="" Address="" UserType="False" DeclArea="VAR_GLOBAL" Len="0" NameOrg="SFCStateAuto" Namespace="Application.GVL_ILD2G" />
      <Variable ID="3" Name="manualforcing" Type="72" Comment="" Address="" UserType="False" DeclArea="VAR_GLOBAL" Len="0" NameOrg="ManualForcing" Namespace="Application.GVL_ILD2G" />
    </UsedVariable>
  </GlobalVariables>
  <IL2G_Names AppName="Application" StrgName="IndraMotionMlc1" LibName="RIL_Diagnosis" />
</DiagnosisCompileData>